<?php 
global $base_url; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>Disknode - <?= $title ?></title>
    <style type="text/css">
    <?php readfile(dirname(__FILE__).'/disknode.style.css'); ?>
    </style>
    <script type="text/javascript">
    function selectPath(selection) {
        elm = window.opener.document.getElementById('edit-filepath');
        if (elm) {
            elm.value = selection;
            elm = document.getElementById('remainOpen');
            if (elm && elm.checked) return;
            window.close();
            return;
        }
        alert('Unable to set selection');
    }

    function setCookie(name, value)
    {
        document.cookie = name+'='+escape(value);
    }
    </script>
    <base href="<?= $base_url ?>/" />
</head>
<body>
<h1><?= $title ?></h1>
<h2><?php 
$relpath = "";
if (!empty($subdir)) $_r = "/".$subdir; else $_r = "";
foreach (explode("/", $_r) as $elm)
{
    if (empty($elm)) $title = "root";
    else $title = $elm;
    if (!empty($relpath)) $relpath .= "/";
    $relpath .= $elm;
    if (strcmp($relpath, variable_get('disknode_base', '')) < 0) echo $title;
    else echo "<a href=\"".url("disknode/browse", "subdir=".$relpath)."\">".$title."</a>";
    echo " / ";
}
?></h2>

<?php

if (!is_writable(file_create_path($subdir))) $dirmodlink = array("class" => "disabled", "title" => "Directory is not writeable");
else $dirmodlink = null;

echo theme_status_messages();
?>
<ul>
<?php

if ($subdir != variable_get('disknode_base', '')) echo "<li><a href=\"".url("disknode/browse", "subdir=".dirname($subdir))."\">..</a></li>";

foreach ($entries as $entry)
{
    if ($entry->isdir)
    {
        echo "<li title=\"Browse directory\"><a href=\"".url("disknode/browse", "subdir=".$entry->filename)."\">$entry->basename</a></li>\n";
    }
    else {
        echo "<li><span onclick=\"selectPath('".addslashes($entry->filename)."')\" ".($entry->dbentry?"class=\"exists\" title=\"This file is already assigned to a node\"":"title=\"Select this file\"").">".$entry->basename."</span>
        <span class=\"filecontrols\">".l("info", "disknode/info", null, "target=".$entry->filename)."</span>
        </li>\n";
    }
}

?>
</ul>

<div id="controls">
<span title="When checked the window will not be closed when an item is selected."><input type="checkbox" id="remainOpen" onclick="setCookie('remainOpen', this.checked);" <?= $_COOKIE["remainOpen"]=="true"?"checked=\"checked\"":"" ?> /><label for="remainOpen">Remain open</label></span>
|
<span title="Upload a file to this directory"><?= l("Upload", "disknode/upload", $dirmodlink, "subdir=".$subdir) ?></span>
|
<span title="Create a new subdirectory"><?= l("Create dir", "disknode/mkdir", $dirmodlink, "subdir=".$subdir) ?></span>
</div>
</body>
</html>