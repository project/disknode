<?php 
global $base_url; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
	<title>Disknode - <?= $title ?></title>
	<style type="text/css">
	<?php readfile(dirname(__FILE__).'/disknode.style.css'); ?>
	</style>
	<script type="text/javascript">
	</script>
	<base href="<?= $base_url ?>/" />
</head>
<body>
<h1><?= $title ?></h1>
<h2>root <?php 
foreach (explode("/", $subdir) as $elm)
{
	if (empty($elm)) continue;
	echo " / ".$elm;
}
?></h2>

<?php

if (!is_writable(file_create_path($subdir))) drupal_set_message("$subdir is not writeable", 'error');
echo theme_status_messages();

echo $form;

?>

<div id="controls">
<span title="Browse and select files"><?= l("Browse", "disknode/browse", null, "subdir=".$subdir) ?></span>
|
<span title="Upload a file to this directory"><?= l("Upload", "disknode/upload", $dirmodlink, "subdir=".$subdir) ?></span>
</div>
</body>
</html>