<?php


function disknode_help($section = "admin/help#weblink") {
  switch ($section) {
    case "admin/help#disknode":
      return t("<p>The diskfile module is used to attach files allready on the system to nodes. It's ideal for hosting downloads.</p>");
    case "admin/modules#description":
      return t("Allows articles with an associated file");
    case 'node/add#disknode':
      return t("Add a new node for a file already on disk. It's ideal for hosting downloads.");
      break;
  }
}

function disknode_settings() {
  // Restrict administration of this module
  if (!user_access('administer')) {
    return message_access();
  }
  $form['disknode_base'] = array(
    '#type' => 'textfield',
    '#title' => t('Base directory'),
    '#default_value' => variable_get('disknode_base', ''),
    '#size' => 72,
    '#maxlength' => 65535,
    '#description' => t('The base directory in the files directory. This is used for the browser, users can not escape the base directory, but are able to link to files outside the base directory (but not outside the files directory).'),
  );
  /*
  $form[] = array(
    '#type' => 'fieldset',
    '#title' => t('Disknode'),
    '#description' => t(''),
  );*/
  return $form;
}

/**
 * Implementation of hook_menu().
 */
function disknode_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'node/add/disknode',
      'title' => t('disknode'),
      'access' => user_access('create disknode'));
    $items[] = array(
      'path' => 'disknode/browse',
      'callback' => 'disknode_browse',
      'type' => MENU_CALLBACK,
      'access' => user_access('create disknode'));
    $items[] = array(
      'path' => 'disknode/get',
      'callback' => 'disknode_get',
      'type' => MENU_CALLBACK,
      'callback arguments' => arg(2),
      'access' => user_access('access content'));
    $items[] = array(
      'path' => 'disknode/upload',
      'callback' => 'disknode_upload',
      'type' => MENU_CALLBACK,
      'access' => user_access('upload files'));
    $items[] = array(
      'path' => 'disknode/mkdir',
      'callback' => 'disknode_mkdir',
      'type' => MENU_CALLBACK,
      'access' => user_access('create directory'));
    $items[] = array(
      'path' => 'disknode/info',
      'callback' => 'disknode_info',
      'type' => MENU_CALLBACK,
      'access' => user_access('create disknode'));
    }
  return $items;
}

function disknode_link($type, $node = 0, $main = 0) {
  $links = array();
  // Node links for a weblink
  if ($type == 'node' && $node->type == 'disknode' && $node->filepath) {
    $links[] = t('Downloads: %counter', array('%counter' => $node->downloads));
    $links[] = l(t('download', array('%title' => $node->title)), "disknode/get/".$node->fid."/".rawurlencode($node->filename), array("title" => "Download ".$node->filename , "rel" => "nofollow" ), "download")." (".format_size($node->filesize).")";
  }
  return $links;
}

function disknode_perm() {
  return array('download files', 'create disknode', 'edit own disknodes', 'upload files', 'create directory');
}

function disknode_node_info() {
  return array('disknode' => array('name' => t('disknode'), 'base' => 'disknode'));
}

function disknode_access($op, $node) {
  global $user;

  switch($op) {
    case 'view': 
      return $node->status;   // see book.module for reference

    case 'create':
      return user_access("create disknode");
  
    case 'delete':
    case 'update':
      return user_access("edit own disknodes") && ($user->uid == $node->uid);
  }
}

function disknode_form(&$node, &$param) {
  $form['title'] = array('#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => $node->title,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE
  );
  $form["filepath"] = array(
    '#type' => 'textfield',
    '#title' => t("File path"),
    '#default_value' => $node->filepath,
    '#size' => 60,
    '#maxlength' => 65535,
    '#description' => t("The relative filepath in the files directory") . ($error['filepath'] ? $error['filepath'] : ''),
    '#attributes' => NULL,
    '#required' => TRUE,
  );
  $form["browsebtn"] = array(
    '#type' => 'button',
    '#button_type' => 'button',
    '#attributes' => array(
      "onclick" => "window.open('".url("disknode/browse", "disknodeutil", NULL, true)."&selection='+document.getElementById('edit-filepath').value, 'filebrowser', 'width=400, height=600, resizable=yes, scrollbars=yes'); return false;",
    ),
    '#value' => 'browse files',
  );
  $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => t("Body"),
    '#default_value' => $node->body,
    '#cols' => 60,
    '#rows' => 10,
    '#description' => t("Textual description of the file") . ($error['body'] ? $error['body'] : ''),
  );
  $form['body_filter']['format'] = filter_form($node->format);

  return $form;
}

function disknode_get($fid, $fname="") {
  $result = db_fetch_object(db_query("SELECT f.filepath, d.counter FROM {files} f LEFT JOIN {downloads} d ON f.fid = d.fid WHERE f.fid=%d", $fid));
  if ($result->counter) {
    db_query("UPDATE {downloads} SET counter = counter + 1 WHERE fid=%d", $fid);
  }
  else {
    db_query("INSERT INTO {downloads} (fid, counter) VALUES (%d, 1)", $fid);
  }
  $url = file_create_url(str_replace("%2F", "/", rawurlencode($result->filepath)));
  if (strstr($url, "?") === false) $url .= "?";
  else $url .= "&";
  $url .= "download";
  header("Location: ".$url);
  exit();
}

function disknode_file_download($file) { 
  if (user_access('download files')) {
    $result = db_query(db_rewrite_sql("SELECT f.nid, f.* FROM {files} f WHERE filepath = '%s'", 'f'), $file);
    if ($file = db_fetch_object($result)) {
      $name = mime_header_encode($file->filename);
      $type = mime_header_encode($file->filemime);
      $disposition = preg_match("#(\?|&)download(&|$)#", $_SERVER["REQUEST_URI"]) ? 'attachment' : 'inline';
      return array('Content-Type: '. $type .'; name='. $name,
                   'Content-Length: '. $file->filesize,
                   'Content-Disposition: '. $disposition .'; filename='. $name);
    }
  }
}

function __sanitize_subdir()
{
  $subdir = $_GET["subdir"];
  if ($subdir == "." || empty($subdir) || (strcmp($subdir, variable_get('disknode_base', '')) < 0)) $subdir = variable_get('disknode_base', '');
  return $subdir;
}

function disknode_browse()
{
  $title = "Browse files";
  $filebase = file_create_path();
  if (!empty($_GET["selection"]))
  {
    $_GET["subdir"] = dirname($_GET["selection"]);
  }
  $subdir = __sanitize_subdir();
  $entries = file_scan_directory(file_create_path($subdir), ".*", array(".", "..", "CVS"), 0, false);
  ksort($entries);
  foreach ($entries as $k => $_)
  {
    $entries[$k]->isdir = is_dir($entries[$k]->filename);
    $entries[$k]->filename = preg_replace("#^(".preg_quote($filebase)."/)#", "", $entries[$k]->filename);
    if (!$entries[$k]->isdir) $entries[$k]->dbentry = db_fetch_object(db_query("SELECT * FROM {files} WHERE filepath = '".$entries[$k]->filename."'"));
    else $entries[$k]->dbentry = false;
  }
  include(dirname(__FILE__)."/disknode.browse.tpl.inc");
}

function disknode_insert($node) {
  $fid = db_next_id('{files}_fid');
  db_query("INSERT INTO {files} (fid, nid, filename, filepath, filemime, filesize, list) VALUES (%d, %d, '%s', '%s', '%s', '%s', %d)",
    $fid, $node->nid, $node->filename, $node->filepath, $node->filemime, $node->filesize, 0);
}

function disknode_update($node) {
  db_query("UPDATE {files} SET filename = '%s', filepath = '%s', filemime = '%s', filesize = '%s' WHERE nid = '%d'", 
    $node->filename, $node->filepath, $node->filemime, $node->filesize, $node->nid);
}

function disknode_delete(&$node) {
  db_query("DELETE FROM {files} WHERE nid=%d", $node->nid);
}

function disknode_submit(&$node) {
  $fp = file_create_path($node->filepath);
  $node->filename = basename($node->filepath);
  $node->filesize = filesize($fp);
  $node->filemime = mime_content_type($fp);
}

function disknode_load(&$node) {
  $result = db_fetch_object(db_query("SELECT f.fid, f.filename, f.filepath, f.filesize, f.filemime, d.counter FROM {files} f LEFT JOIN {downloads} d ON f.fid = d.fid WHERE nid=%d", $node->nid));
  $node->fid = $result->fid;
  $node->filename = $result->filename;
  $node->filepath = $result->filepath;
  $node->filesize = $result->filesize;
  $node->filemime = $result->filemime;
  $node->downloads = intval($result->counter);
}

function disknode_upload() {
  $title = "Upload file";
  $subdir = __sanitize_subdir();
  if ($file = file_check_upload('file')) {
    $dest = $subdir."/".$file->filename;
    $file = file_save_upload('file', $dest, $_POST["edit"]["overwrite1"]=="1"?FILE_EXISTS_REPLACE:FILE_EXISTS_ERROR);
    if ($file) {
      drupal_set_message("File saved to $file->filepath", 'status');
    }
    else {
      drupal_set_message("Failed to save $dest", 'error');
    }
  }
  if (!empty($_POST["edit"]["url"])) {
    $file = _disknode_downloadfile($_POST["edit"]["url"]);
    if (empty($file->error))
    {
      $bname = preg_replace("#^(.*)(\?(.*))?$#iU", "\\1", basename($_POST["edit"]["url"]));
      $dest = $subdir."/".$bname;
      if (file_move($file, $dest, $_POST["edit"]["overwrite2"]=="1"?FILE_EXISTS_REPLACE:FILE_EXISTS_ERROR)) {
        drupal_set_message("File saved to $file->filepath");
      }
      else {
        drupal_set_message("Failed to save $dest", 'error');
      }
    }
    else {
      drupal_set_message("Download error: ".$file->error, 'error');
    }
  }

  $form1['file'] = array(
    '#type' => 'file',
    '#title' => t('File'),
    '#size' => 40,
    '#description' => t('Click "Browse..." to select an file to upload.'),
  );
  $form1['overwrite1'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overwrite'),
    '#return_value' => 1,
    '#default_value' => false,
    '#description' => t('Overwrite the file if it exists.'),
  );
  $form1[] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );

  $form1['#method'] = 'POST';
  $form1['#action'] = NULL;
  $form1['#attributes'] = array('enctype' => 'multipart/form-data');
  $form = drupal_get_form('uploadForm', $form1);

  $form2['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Download URL'),
    '#default_value' => '',
    '#size' => 50,
    '#maxlength' => 65535,
    '#description' => t('Enter the url to download the file from. NOTE: this could take a while.'),
  );
  $form2['overwrite2'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overwrite'),
    '#return_value' => 1,
    '#default_value' => false,
    '#description' => t('Overwrite the file if it exists.'),
  );
  $form2[] = array(
    '#type' => 'submit',
    '#value' => t('Download'),
  );
  $form2['#method'] = 'POST';
  $form .= drupal_get_form('downloadForm', $form2);

  include(dirname(__FILE__)."/disknode.upload.tpl.inc");
}

function disknode_mkdir() {
  $title = "Create directory";
  $subdir = __sanitize_subdir();

  if (!empty($_POST["edit"]["dirname"])) {
    if (!empty($subdir)) $subdir .= "/";
    $dirname = file_create_path($subdir.$_POST["edit"]["dirname"]);
    //echo $dirname;
    if (!file_check_directory($dirname , FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS))
    {
      drupal_set_message("Failed to create\\update $dirname", 'error');
    }
  }

  $form['dirname'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory name'),
    '#default_value' => '',
    '#size' => 50,
    '#maxlength' => 65535,
    '#description' => t('Enter the name of the directory.'),
  );
  $form[] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );
  $form['#method'] = 'POST';
  $form = drupal_get_form('your_form_id', $form);

  include(dirname(__FILE__)."/disknode.upload.tpl.inc");
}

function disknode_info() {
  $title = "File information";
  $subdir = __sanitize_subdir();

  $file = new StdClass();
  $file->filename = $_GET["target"];

  include(dirname(__FILE__)."/disknode.info.tpl.inc");
}

/* 
  not OS safe
*/
if (!function_exists("mime_content_type")) {
  function mime_content_type($f) 
  {
    $f = escapeshellarg($f);
    return trim( exec("file -bi ".$f) );
  }
}

function _disknode_downloadfile($url){
  define("DOWNLOAD_CHUNK", 8192);

  $result = new StdClass();
  $result->url = $url;
  $result->size = 0;
  $result->error = "";

  $inp = @fopen($url, "rb");
  if (!$inp) {
    $result->error = "Unable to open ".$url;
    drupal_set_message(t($result->error), 'error');
    return $result;
  }
  $result->filepath = tempnam(file_create_path(variable_get('file_directory_temp', FILE_DIRECTORY_TEMP)), "disknode_");
  $outp = fopen($result->filepath, "wb");
  if (!$outp) {
    fclose($inp);
    $result->error = "Unable to open temporary file ".$result->filepath;
    drupal_set_message(t($result->error), 'error');
    return $result;
  }
  ignore_user_abort(true);
  set_time_limit(0); // no limit

  while (!feof($inp))
  {
    $result->size += fwrite($outp, fread($inp, DOWNLOAD_CHUNK));
  }
  fclose($inp);
  fclose($outp);
  return $result;
}

