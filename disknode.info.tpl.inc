<?php 
global $base_url; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>Disknode - <?= $title ?></title>
    <style type="text/css">
    <?php readfile(dirname(__FILE__).'/disknode.style.css'); ?>
    </style>
    <base href="<?= $base_url ?>/" />
</head>
<body>
<h1><?= $title ?></h1>
<h2><?= basename($file->filename) ?></h2>

<i>Not implemented</i>

<div id="controls">
<span title="Browse and select files"><?= l("Browse", "disknode/browse", null, "subdir=".dirname($file->filename)) ?></span>
</div>
</body>
</html>